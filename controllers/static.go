package controllers

import (
	"lenslocked.com/views"
)

// NewStatic function - renders static pages
func NewStatic() *Static {
	return &Static{
		Home:    views.NewView("bootstrap", "static/home"),
		Contact: views.NewView("bootstrap", "static/contact"),
	}
}

// Static struct - creates static object
type Static struct {
	Home    *views.View
	Contact *views.View
}
