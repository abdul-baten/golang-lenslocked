package main

import (
	"fmt"
	"net/http"
	"time"

	"lenslocked.com/models"

	"github.com/gorilla/mux"
	"lenslocked.com/controllers"
)

const (
	postgreSQLURI = "postgres://nhkuvhux:dkTpO_dZURcxYour2XS7LmDBicQaQCtE@isilo.db.elephantsql.com:5432/nhkuvhux"
)

func notFound(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	w.WriteHeader(http.StatusNotFound)

	fmt.Fprint(w, "<h1>Sorry, but we couldn't find what you're looking for!</h1>")
}

func main() {

	db, err := models.NewUserService(postgreSQLURI)
	if err != nil {
		panic(err)
	}

	defer db.Close()

	staticC := controllers.NewStatic()
	usersC := controllers.NewUsers(db)

	r := mux.NewRouter()

	r.Handle("/", staticC.Home).Methods("GET")
	r.Handle("/contact", staticC.Contact).Methods("GET")
	r.Handle("/signup", usersC.UserView).Methods("GET")
	r.HandleFunc("/signup", usersC.Create).Methods("POST")
	r.Handle("/login", usersC.LoginView).Methods("GET")
	r.HandleFunc("/login", usersC.Login).Methods("POST")

	r.NotFoundHandler = http.HandlerFunc(notFound)

	server := http.Server{
		Addr:         ":3000",
		Handler:      r,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	server.ListenAndServe()
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
