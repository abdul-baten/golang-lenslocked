package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var (
	db  *gorm.DB
	err error
)

const (
	postgreSQLURI = "postgres://nhkuvhux:dkTpO_dZURcxYour2XS7LmDBicQaQCtE@isilo.db.elephantsql.com:5432/nhkuvhux"
)

type User struct {
	gorm.Model
	Name         string
	Email        string `gorm:"not null;unique_index"`
	Password     string `gorm:"-"`
	PasswordHash string `gorm:"not null"`
}

// type Order struct {
// 	gorm.Model
// 	UserID      uint // Foreign key related to User
// 	Amount      int
// 	Description string
// }

func main() {

	db, err = gorm.Open("postgres", postgreSQLURI)
	if err != nil {
		panic(err)
	}

	defer db.Close()

	if err := db.DB().Ping(); err != nil {
		panic(err)
	}

	db.LogMode(true)

	db.DropTableIfExists(&User{})
	db.AutoMigrate(&User{})
}
